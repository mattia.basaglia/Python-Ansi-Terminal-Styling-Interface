#!/usr/bin/python

import sys
import subprocess
import argparse

from patsi.ansi import SGR
from patsi.document.color import RgbColor
from patsi.widgets.geometry import Point
from patsi.document.loader._utils import string_to_color

def terminal_size():
    try:
        return Point(*map(int,
            subprocess.Popen(["stty", "size"],stdout=subprocess.PIPE)
            .communicate()[0].strip().split(" ")
        ), reversed=True)
    except:
        return Point(80, 24)


def color_cell(color):
    sys.stdout.write("%s " % SGR(SGR.ColorRGB(*color.rgb_tuple, background=True)))


def endline():
    sys.stdout.write("%s\n" % SGR())


class MapMode(object):
    def __init__(self, *colors):
        if not colors:
            self.fixed = 1
        else:
            self.fixed = sum(map(self._fixed, colors)) / len(colors)

    def _fixed(self, colors):
        raise NotImplementedError()

    def color(self, rx, ry):
        raise NotImplementedError()

    def color_rect(self, size):
        for y in xrange(size.y):
            ry = float(y) / size.y
            for x in xrange(size.x):
                rx = float(x) / size.x
                yield self.color(rx, ry)
            endline()


class ModeHueSat(MapMode):
    def _fixed(self, color):
        return color.hsv()[2]

    def color(self, rx, ry):
        return RgbColor.from_hsv(rx, ry, self.fixed)


class ModeHueVal(MapMode):
    def _fixed(self, color):
        return color.hsv()[1]

    def color(self, rx, ry):
        return RgbColor.from_hsv(rx, self.fixed, ry)


def visualize_distance(size, refcol, dist, mode_class=ModeHueVal):
    mode = mode_class(refcol)
    for color in mode.color_rect(size):
        cell_color = color
        distance = refcol.distance(color)
        if distance < dist:
            cell_color = refcol
        color_cell(cell_color)


def partition(size, colors, mode_class=ModeHueVal):
    mode = mode_class(*colors)
    for color in mode.color_rect(size):
        partitioned = color
        min_distance = None
        for input in colors:
            distance = input.distance(color)
            if min_distance is None or distance < min_distance:
                min_distance = distance
                partitioned = input
        color_cell(partitioned)


def preview_color(color, name=None):
    if name is None:
        name = color.name
    print "%s%s%s" % (
        SGR(SGR.ColorRGB(*color.rgb_tuple, background=True), SGR.Black),
        name,
        SGR(),
    )


def parse_args():
    parser = argparse.ArgumentParser(
        description="Shows a map of similar colors",
        add_help=False,
    )

    parser.add_argument(
        "--width", "-w",
        type=int,
        default=None
    )

    parser.add_argument(
        "--height", "-h",
        type=int,
        default=None
    )

    parser.add_argument(
        "--distance", "-d",
        type=int,
        default=32
    )

    parser.add_argument(
        "colors",
        nargs='*'
    )

    parser.add_argument(
        "--help", "-?",
        action="help",
    )

    return parser.parse_args()


args = parse_args()

colors = [string_to_color(color).rgb for color in args.colors]
for color in colors:
    preview_color(color)

size = terminal_size()
if args.width is not None:
    size.x = args.width

if args.height is not None:
    size.y = args.height

if len(colors) == 1:
    visualize_distance(size, colors[0], args.distance)
else:
    partition(size, colors)
