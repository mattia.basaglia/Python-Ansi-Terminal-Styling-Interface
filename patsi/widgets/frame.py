from .style import Style

class Frame(object):
    def __init__(self, style=Style(),
                 top="-", bottom="-", left="|", right="|",
                 tl="+", bl="+", br="+", tr="+"):
        self.style = style

        self.top = top
        self.bottom = bottom
        self.left = left
        self.right = right

        self.tl = tl
        self.bl = bl
        self.br = br
        self.tr = tr

    def draw(self, painter, rect):
        """
        Draws the frame manually, might not be the most efficient depending on the painter
        """
        painter.set_style(self.style)
        # Top left
        painter.draw_text_at(rect.x, rect.y, self.tl)
        # Top
        painter.draw_text_at(
            rect.x + len(self.tl),
            rect.y,
            self.top * ((rect.width - len(self.tl) - len(self.tr)) / len(self.top))
        )
        # Top right
        painter.draw_text_at(rect.right - len(self.tr), rect.y, self.tr)
        # Right
        right_x = rect.right - len(self.right)
        for i in range(rect.height - 2):
            painter.draw_text_at(right_x, rect.y + 1 + i, self.right)
        # Bottom right
        painter.draw_text_at(rect.right - len(self.br), rect.bottom - 1, self.br)
        # Bottom
        painter.draw_text_at(
            rect.x + len(self.bl),
            rect.bottom - 1,
            self.bottom * ((rect.width - len(self.bl) - len(self.br)) / len(self.bottom))
        )
        # Bottom left
        painter.draw_text_at(rect.x, rect.bottom - 1, self.bl)
        # Left
        for i in range(rect.height - 2):
            painter.draw_text_at(rect.x, rect.y + 1 + i, self.left)
