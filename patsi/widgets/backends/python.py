#
# Copyright (C) 2016-2017 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import time
import fcntl
import struct
import termios

from ... import ansi
from ...input import RawInput
from ...document.color import IndexedColor, RgbColor, UnchangedColor

from ..event import Event
from ..geometry import Rect, Point
from ..style import Style

from . import base


def term_size(self, renderer, default=Point(80, 24)):
    try:
        h, w = struct.unpack("hh",
            fcntl.ioctl(renderer.output.fileno(), termios.TIOCGWINSZ, b"\x00" * 4)
        )
        return Point(h, w)
    except:
        return default


class Painter(base.Painter):
    def __init__(self, renderer, widget, pos=None, style=None):
        self.renderer = renderer
        self.widget = widget
        self.pos = pos or Point(0, 0)
        if isinstance(style, ansi.GraphicRendition):
            self.sgr = style
        else:
            self.set_style(style)

    def _flag_to_ansi(self, flag):
        if flag == -1:
            return 22
        if flag < 0:
            return ansi.GraphicRendition.reverse(flag)
        return flag

    def _color_to_ansi(self, color):
        if type(color) is int:
            return [30 + color]
        if color is UnchangedColor:
            return []
        if isinstance(color, RgbColor):
            return [38, 2, color.r, color.g, color.b]
        if isinstance(color, IndexedColor):
            if len(color.palette) > 8:
                return [38, 5, color.index]
            return 30 + color.index

    def _style_to_ansi(self, style):
        if not style:
            return ansi.GraphicRendition()

        return ansi.GraphicRendition(
            map(self._flag_to_ansi, style.styles)
            + self._color_to_ansi(style.color)
        )

    def set_style(self, style):
        self.sgr = self._style_to_ansi(style)
        self.renderer.ansi(self.sgr)

    def set_pos(self, pos):
        self.pos = pos
        self.renderer.move_cursor(x=pos.x, y=pos.y)

    def _draw_border(self, frame):
        frame.draw(self, self.widget._geometry)

    def draw_text(self, text):
        self.renderer.write(text)

    def draw_styled_text(self, pos, style, text):
        self.renderer.ansi(self.sgr)
        self.renderer.move_cursor(x=pos.x, y=pos.y)
        self.renderer.write(text)

    def _draw_text_at(self, pos, text):
        self.renderer.move_cursor(x=pos.x, y=pos.y)
        self.draw_text(text)
        self.renderer.move_cursor(x=self.pos.x, y=self.pos.y)

    def _get_child_painter(self, child_widget):
        return Painter(self.renderer, child_widget, self.pos, self.sgr)

    def __enter__(self):
        if not self.widget.parent:
            self.renderer.clear()
        return self


class Backend(base.Backend):
    """
    Pure python ansi back-end
    """
    def __init__(self, renderer=None, frame_interval=0.5, input=None):
        self.renderer = renderer or ansi.AnsiRenderer()
        self.frame_interval = frame_interval
        self.input = input or RawInput(raise_specials=False)
        self.term_size = Point(80, 24)
        self.context = [
            self.renderer.rendering_context(),
            self.input
        ]

    def setup_widget(self, widget):
        if widget.parent is not None:
            widget.geometry = widget.geometry_hint(widget.parent.geometry)
        else:
            widget.geometry = Rect(x=0, y=0, size=self.term_size)

    def get_geometry(self, widget):
        return widget._geometry

    def set_geometry(self, widget, geometry):
        widget._geometry = geometry

    def listen_events(self, widget):
        new_size = term_size(self.renderer, widget.geometry.size)
        if new_size != widget.geometry.size:
            widget.event(Event.resize_event(Rect(pos=widget.geometry.pos, size=new_size)))

        ch = self.input.get()
        if ch:
            if ch == '\3' or ch == '\4':
                widget.event(Event.close_event(ord(ch)))
            elif len(ch) == 1 and 32 >= ord(ch) < 127:
                # Printable ASCII
                widget.event(Event.text_event(ch))
            else:
                widget.event(Event.key_event(ch))

        if widget.enabled:
            time.sleep(self.frame_interval)

    def painter(self, widget):
        return Painter(self.renderer, widget)

    def __enter__(self):
        for ctx in self.context:
            ctx.__enter__()
        self.term_size = term_size(self.term_size)

    def show_cursor(self, show):
        self.renderer.ansi(ansi.common.show_cursor if show else ansi.common.hide_cursor)

    def __exit__(self, *a, **k):
        for ctx in reversed(self.context):
            ctx.__exit__(*a, **k)
