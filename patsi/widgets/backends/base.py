#
# Copyright (C) 2016-2017 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from ..frame import Frame
from ..geometry import Point


class Painter(object):
    def __enter__(self):
        return self

    def __exit__(self, *a, **k):
        pass

    def set_style(self, style):
        raise NotImplementedError()

    def set_pos(self, pos):
        raise NotImplementedError()
    def draw_border(self, frame=Frame()):
        self._draw_border(frame)

    def _draw_border(self, frame):
        raise NotImplementedError()

    def draw_styled_text(self, pos, style, text):
        raise NotImplementedError()

    def draw_text(self, text):
        raise NotImplementedError()

    def draw_text_at(self, *args):
        if len(args) == 3:
            self._draw_text_at(Point(args[0], args[1]), args[2])
        else:
            self._draw_text_at(args[0], args[1])

    def _draw_text_at(self, pos, text):
        raise NotImplementedError()

    def _get_child_painter(self, child_widget):
        raise NotImplementedError()

    def enter(self, child_widget):
        return self._get_child_painter(child_widget)


class Backend(object):
    """
    Backend class

    NOTE: Specializations must define the constants KEY_* for special keys
    """

    def setup_widget(self, widget):
        raise NotImplementedError()

    def get_geometry(self, widget):
        raise NotImplementedError()

    def set_geometry(self, widget, geometry):
        raise NotImplementedError()

    def listen_events(self, widget):
        raise NotImplementedError()

    def painter(self, widget):
        raise NotImplementedError()

    def init(self):
        raise NotImplementedError()

    def deinit(self):
        raise NotImplementedError()

    def show_cursor(self, show):
        raise NotImplementedError()

    def __enter__(self):
        self.init()
        return self

    def __exit__(self, *a, **k):
        self.deinit()
