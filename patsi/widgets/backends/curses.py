#
# Copyright (C) 2016-2017 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from __future__ import absolute_import

import curses
import curses.ascii

from ..geometry import Point, Rect
from ..event import Event
from ..style import Style

from . import base


class CursesPainter(base.Painter):
    style_map = {
        Style.STYLE_BOLD: curses.A_BOLD,
        Style.STYLE_FAINT: curses.A_DIM,
        Style.STYLE_ITALIC: curses.A_UNDERLINE,
        Style.STYLE_UNDERLINE: curses.A_UNDERLINE,
        Style.STYLE_BLINK: curses.A_BLINK,
        Style.STYLE_REVERSE: curses.A_REVERSE,
    }

    def __init__(self, window, style=None, pos=None):
        self.window = window
        self.style = style or Style()
        self.pos = pos or Point()

    def set_style(self, style):
        self.style = style

    def set_pos(self, pos):
        self.pos = pos

    def __enter__(self):
        self.window.clear()
        return self

    def __exit__(self, *a, **k):
        self.window.refresh()

    def color_to_curses(self, color):
        if color is None:
            return curses.A_NORMAL | curses.color_pair(0)

        try:
            index = color.index
        except AttributeError:
            index = color
        flags = curses.color_pair((index & 7) + 1)
        if index & 8:
            flags |= curses.A_BOLD
        return flags

    def style_to_curses(self, style):
        mode = 0
        for style_flag in style.styles:
            if style_flag == Style.STYLE_NORMAL:
                mode = 0
            elif style_flag > 0:
                mode |= self.style_map[style_flag]
            else:
                mode &= ~self.style_map[-style_flag]
        mode |= self.color_to_curses(style.color)
        return mode

    def draw_styled_text(self, pos, style, text):
        self.window.addstr(pos.y, pos.x, text, self.style_to_curses(style))

    def _draw_border(self, frame):
        self.window.border(
            frame.left,
            frame.right,
            frame.top,
            frame.bottom,
            frame.tl,
            frame.tr,
            frame.bl,
            frame.br
        )

    def draw_text(self, text):
        self.draw_styled_text(self.pos, self.style, text)

    def _draw_text_at(self, pos, text):
        self.draw_styled_text(pos, self.style, text)

    def _get_child_painter(self, child_widget):
        return CursesPainter(child_widget._backend_window, self.style, self.pos)


class CursesBackend(base.Backend):
    KEY_LEFT = curses.KEY_LEFT
    KEY_RIGHT = curses.KEY_RIGHT
    KEY_UP = curses.KEY_UP
    KEY_DOWN = curses.KEY_DOWN
    KEY_ENTER = 13
    KEY_BACKSPACE = curses.KEY_BACKSPACE
    KEY_HOME = curses.KEY_HOME
    KEY_END = curses.KEY_END
    KEY_F1 = curses.KEY_F1
    KEY_SHOME = curses.KEY_SHOME
    KEY_DC = curses.KEY_DC

    mouse_events = {"PRESSED", "RELEASED", "CLICKED", "DOUBLE_CLICKED", "TRIPLE_CLICKED",}

    def __init__(self):
        self.screen = None

    def setup_widget(self, widget):
        if widget.parent is not None:
            bounds = widget.geometry_hint(widget.parent.geometry)
            if getattr(widget, "_backend_window", None) is None:
                widget._backend_window = widget.parent._backend_window.subwin(
                    bounds.height, bounds.width, bounds.y, bounds.x
                )
            else:
                self.set_geometry(widget, bounds)
        elif self.screen:
            widget._backend_window = self.screen
        else:
            widget._backend_window = curses.newwin(24, 80)

    def get_geometry(self, widget):
        return Rect(
            top_left=Point(*widget._backend_window.getbegyx(), reversed=True),
            bottom_right=Point(*widget._backend_window.getmaxyx(), reversed=True)
        )

    def set_geometry(self, widget, geometry):
        if widget.geometry != geometry:
            widget._backend_window.mvwin(geometry.y, geometry.x)
            widget._backend_window.resize(geometry.height, geometry.width)

    def listen_events(self, widget):
        ch = widget._backend_window.getch()
        event = None

        if ch == curses.KEY_RESIZE:
            event = Event.resize_event(self.get_geometry(widget))
        elif ch == curses.KEY_MOUSE:
            try:
                id, x, y, z, bstate = curses.getmouse()
                event = Event.mouse_event(Point(x, y), bstate)
            except curses.error:
                pass
        elif curses.ascii.isprint(ch):
            event = Event.text_event(chr(ch))
        else:
            event = Event.key_event(ch)

        if event is not None:
            widget.event(event)

    def painter(self, widget):
        return CursesPainter(widget._backend_window, None, None)

    def show_cursor(self, show):
        curses.curs_set(1 if show else 0)

    def init(self):
        self.screen = curses.initscr()
        curses.noecho()
        curses.cbreak()
        curses.nonl()
        self.screen.keypad(1)
        curses.start_color()
        curses.use_default_colors()

        curses.init_pair(1, curses.COLOR_BLACK,     -1)
        curses.init_pair(2, curses.COLOR_RED,       -1)
        curses.init_pair(3, curses.COLOR_GREEN,     -1)
        curses.init_pair(4, curses.COLOR_YELLOW,    -1)
        curses.init_pair(5, curses.COLOR_BLUE,      -1)
        curses.init_pair(6, curses.COLOR_MAGENTA,   -1)
        curses.init_pair(7, curses.COLOR_CYAN,      -1)
        curses.init_pair(8, curses.COLOR_WHITE,     -1)

        curses.mousemask(curses.ALL_MOUSE_EVENTS)

    def deinit(self):
        self.screen.keypad(0)
        curses.nl()
        curses.nocbreak()
        curses.echo()
        curses.endwin()
