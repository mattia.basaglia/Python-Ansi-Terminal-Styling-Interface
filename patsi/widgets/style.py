class Style(object):
    STYLE_NORMAL = 0
    STYLE_BOLD = 1
    STYLE_FAINT = 2
    STYLE_ITALIC = 3
    STYLE_UNDERLINE = 4
    STYLE_BLINK = 5
    STYLE_REVERSE = 7

    # Color is patsi.document.color.IndexedColor, an integer (the index), or None
    def __init__(self, styles=[], color=None):
        self.styles = self._make_unique(styles)
        self.color = color

    def __ior__(self, other):
        self.styles = self._make_unique(self.styles, other.styles)
        self.color = other.color
        return self

    def __or__(self, other):
        return Style(self._make_unique(self.styles, other.styles), other.color)

    def _make_unique(self, *styles_list):
        """
        Returns an unambiguous list of styles
        """
        result = []
        def remove(*values):
            for value in values:
                try:
                    result.remove(value)
                except:
                    pass

        for styles in styles_list:
            for style in styles:
                if style == self.STYLE_NORMAL:
                    result = [self.STYLE_NORMAL]
                    continue
                elif style in result:
                    pass
                elif style == self.STYLE_BOLD:
                    remove(self.STYLE_FAINT, -self.STYLE_FAINT, -self.STYLE_BOLD)
                    result.append(style)
                else:
                    remove(-style)
                    result.append(style)

        return result
