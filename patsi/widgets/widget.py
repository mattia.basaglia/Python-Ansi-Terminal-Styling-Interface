#
# Copyright (C) 2016-2017 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from patsi.widgets.geometry import Point, Rect
from .event import Event


class Widget(object):
    def __init__(self, parent, backend=None):
        self.parent = parent
        self.children = []
        self.focused_child = None
        self.enabled = True
        if not parent and not backend:
            raise TypeError("You must specify a parent or a backend")
        self.backend = backend or parent.backend
        self.backend.setup_widget(self)
        if self.parent:
            self.parent.children.append(self)

    def focus(self, *args):
        if not self.enabled:
            return

        if len(args) == 0 or args[0] is self:
            if self.parent:
                self.parent.focus(self)
            return

        if self.focused_child == args[0]:
            return

        if self.focused_child:
            self.focused_child.event(Event.focus_event(False))

        self.focused_child = args[0]

        if self.focused_child:
            self.focused_child.event(Event.focus_event(True))

    def has_focus(self):
        return self.enabled and (not self.parent or self.parent.focused_child == self)

    @property
    def geometry(self):
        return self.backend.get_geometry(self)

    @geometry.setter
    def geometry(self, geometry):
        self.backend.set_geometry(self, geometry)

    def geometry_hint(self, parent_bounds):
        return parent_bounds

    def loop(self):
        while self.enabled:
            self.refresh()
            self.backend.listen_events(self)

    def deactivate(self):
        self.enabled = False

    def event(self, event):
        if event.type == Event.TYPE_RESIZE:
            self.resize_event(event)
            if event.propagate:
                bounds = self.geometry
                for child in self.children:
                    old_bounds = child.geometry
                    new_bounds = child.geometry_hint(bounds)
                    if old_bounds != new_bounds:
                        child.event(Event.resize_event(new_bounds))
            return

        if not self.enabled:
            return

        if event.type == Event.TYPE_TEXT:
            self.text_event(event)
            if self.focused_child and event.propagate:
                self.focused_child.event(event)
        elif event.type == Event.TYPE_KEY:
            self.key_event(event)
            if self.focused_child and event.propagate:
                self.focused_child.event(event)
        elif event.type == Event.TYPE_MOUSE:
            self.mouse_event(event)
            if not event.propagate:
                return
            for child in self.children:
                if child.enabled and child.geometry.contains(event.pos):
                    if self.event_change_focused_child(child):
                        child_bounds = child.geometry
                        relative = event.pos - child_bounds.pos
                        child_event = Event.mouse_event(relative, event.buttons)
                        child.event(child_event)
                        if not child_event.propagate:
                            return
        elif event.type == Event.TYPE_FOCUS:
            self.focus_event(event)

    def event_change_focused_child(self, new):
        self.focused_child = new
        return True

    def text_event(self, event):
        pass

    def key_event(self, event):
        pass

    def resize_event(self, event):
        self.geometry = event.geometry

    def mouse_event(self, event):
        self.focus()

    def refresh(self):
        with self.backend.painter(self) as painter:
            self.render(painter)
        for child in self.children:
            if child.enabled:
                child.refresh()

    def render(self, painter):
        pass

    def focus_event(self, event):
        if event.focus:
            self.refresh()
