#
# Copyright (C) 2016-2017 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import six

class Event(object):
    TYPE_TEXT       = 1
    TYPE_KEY        = 2
    TYPE_RESIZE     = 3
    TYPE_MOUSE      = 4
    TYPE_FOCUS      = 5
    TYPE_CLOSE      = 6

    def __init__(self, type, **kwargs):
        self.type = type
        self.propagate = True
        for attr, val in six.iteritems(kwargs):
            setattr(self, attr, val)

    def accept(self):
        self.propagate = False

    @staticmethod
    def text_event(char):
        return Event(Event.TYPE_TEXT, char=char)

    @staticmethod
    def key_event(key):
        return Event(Event.TYPE_KEY, key=key)

    @staticmethod
    def resize_event(geometry):
        return Event(Event.TYPE_RESIZE, geometry=geometry)

    @staticmethod
    def mouse_event(pos, buttons=None, clicked=None):
        return MouseEvent(pos=pos, buttons=buttons, clicked=clicked)

    @staticmethod
    def focus_event(focus):
        return Event(Event.TYPE_FOCUS, focus=focus)

    @staticmethod
    def close_event(reason_code=0):
        return Event(Event.TYPE_CLOSE, reason_code=reason_code)


class MouseEvent(Event):
    MOUSE_RELEASED       = 0x01
    MOUSE_PRESSED        = 0x02
    MOUSE_CLICKED        = 0x04
    MOUSE_DOUBLE_CLICKED = 0x08
    MOUSE_TRIPLE_CLICKED = 0x10
    _EVENT_MASK          = 0xff

    MODIFIER_CTRL  = 0x1000000
    MODIFIER_SHIFT = 0x2000000
    MODIFIER_ALT   = 0x4000000

    @staticmethod
    def make_flag(button, event_flag):
        return event_flag << MouseEvent.button_shift(button)

    @staticmethod
    def button_shift(button):
        return (button-1) * 6

    def __init__(self, pos, buttons=None, clicked=None):
        if clicked and not buttons:
            buttons = 0
            for button in clicked:
                buttons |= MouseEvent.make_flag(button, MouseEvent.MOUSE_CLICKED)
        super(MouseEvent, self).__init__(self.TYPE_MOUSE, pos=pos, buttons=buttons)

    def button(self, button):
        return (self.buttons >> MouseEvent.button_shift(button)) & self._EVENT_MASK

    def clicked(self, button):
        return bool(self.buttons & MouseEvent.make_flag(button, MouseEvent.MOUSE_CLICKED))
