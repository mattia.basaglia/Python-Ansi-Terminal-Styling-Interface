#
# Copyright (C) 2016-2017 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import curses
from contextlib import contextmanager
from mock import patch, MagicMock
import test_common

from patsi.document import color, palette
from patsi import widgets
from patsi.widgets import geometry as geo
from patsi.widgets.backends.curses import CursesPainter, CursesBackend


class TestCursesPainter(test_common.PatchedTestCase):
    patches = [
        patch("curses.color_pair", side_effect=lambda x: 256 << (x-1) if x else 0)
    ]

    def _setup(self):
        self.painter = CursesPainter(MagicMock())

    def test_color_to_curses(self):
        self.assertEqual(
            curses.A_NORMAL | curses.color_pair(0),
            self.painter.color_to_curses(None)
        )

        self.assertEqual(
            curses.A_NORMAL | curses.color_pair(2),
            self.painter.color_to_curses(color.IndexedColor(1, palette.colors16))
        )

        self.assertEqual(
            curses.A_BOLD | curses.color_pair(2),
            self.painter.color_to_curses(color.IndexedColor(8|1, palette.colors16))
        )

    def test_style_to_curses_normal_clears(self):
        self.assertEqual(
            curses.A_NORMAL,
            self.painter.style_to_curses(widgets.Style(
                [widgets.Style.STYLE_REVERSE, widgets.Style.STYLE_NORMAL]
            ))
        )

    def test_style_to_curses_flags_added(self):
        self.assertEqual(
            curses.A_BOLD|curses.A_UNDERLINE,
            self.painter.style_to_curses(widgets.Style(
                [widgets.Style.STYLE_BOLD, widgets.Style.STYLE_UNDERLINE]
            ))
        )

    def test_style_to_curses_flags_removed(self):
        self.assertEqual(
            curses.A_UNDERLINE,
            self.painter.style_to_curses(widgets.Style(
                [widgets.Style.STYLE_BOLD, widgets.Style.STYLE_UNDERLINE,
                 -widgets.Style.STYLE_BOLD]
            ))
        )

    def test_style_to_curses_color(self):
        self.assertEqual(
            curses.color_pair(4),
            self.painter.style_to_curses(widgets.Style(
                [], 3
            ))
        )

    def test_style_to_curses_color_and_flags(self):
        self.assertEqual(
            curses.A_UNDERLINE | curses.color_pair(4),
            self.painter.style_to_curses(widgets.Style(
                [widgets.Style.STYLE_UNDERLINE], 3
            ))
        )

    def test_draw_styled_text(self):
        style = widgets.Style([widgets.Style.STYLE_UNDERLINE])
        self.painter.draw_styled_text(geo.Point(1,2), style, "foo")
        self.painter.window.addstr.assert_called_with(
            2, 1, "foo", curses.A_UNDERLINE
        )

    def test_draw_border(self):
        self.painter.draw_border()
        self.painter.window.border.assert_called_with(
            '|', '|', '-', '-', '+', '+', '+', '+',
        )

    def test_draw_text(self):
        style = widgets.Style([widgets.Style.STYLE_UNDERLINE])
        self.painter.set_style(style)
        self.painter.set_pos(geo.Point(1,2))
        self.painter.draw_text("foo")
        self.painter.window.addstr.assert_called_with(
            2, 1, "foo", curses.A_UNDERLINE
        )

    def test_draw_text_at(self):
        style = widgets.Style([widgets.Style.STYLE_UNDERLINE])
        self.painter.set_style(style)
        self.painter.set_pos(geo.Point(10,20))
        self.painter.draw_text_at(geo.Point(1,2), "foo")
        self.painter.window.addstr.assert_called_with(
            2, 1, "foo", curses.A_UNDERLINE
        )
        self.painter.draw_text_at(3, 4, "foo")
        self.painter.window.addstr.assert_called_with(
            4, 3, "foo", curses.A_UNDERLINE
        )

    def test_enter(self):
        child = MagicMock()
        with self.painter.enter(child) as child_painter:
            self.assertIs(child_painter.window, child._backend_window)

    def test_context_manager(self):
        self.painter.window.clear.assert_not_called()
        with self.painter as painter:
            self.assertIs(painter, self.painter)
            self.painter.window.clear.assert_called_once()
            self.painter.window.refresh.assert_not_called()
        self.painter.window.refresh.assert_called_once()


class TestCursesBackend(test_common.TestCase):
    def test_curses_context(self):
        backend = CursesBackend()
        self.assertRaises(curses.error, lambda: curses.color_pair(1))
        with backend:
            self.assertFalse(curses.isendwin())
        self.assertTrue(curses.isendwin())

    @patch("curses.newwin")
    def test_setup_widget_root_screen(self, newwin):
        backend = CursesBackend()
        backend.screen = MagicMock()
        widget = MagicMock()
        widget.parent = None
        backend.setup_widget(widget)
        self.assertIs(widget._backend_window, backend.screen)
        newwin.assert_not_called()

    @patch("curses.newwin")
    def test_setup_widget_root_noscreen(self, newwin):
        backend = CursesBackend()
        widget = MagicMock()
        widget.parent = None
        backend.setup_widget(widget)
        newwin.assert_called_once()

    def test_setup_widget_child_new(self):
        backend = CursesBackend()
        parent = MagicMock()
        widget = MagicMock()
        widget.parent = parent
        widget._backend_window = None
        backend.setup_widget(widget)
        parent._backend_window.subwin.assert_called_once()
        widget._backend_window.mvwin.assert_not_called()
        widget._backend_window.resize.assert_not_called()

    def test_setup_widget_child_existing_resize(self):
        backend = CursesBackend()
        parent = MagicMock()
        widget = MagicMock()
        widget._backend_window = MagicMock()
        widget.parent = parent
        widget.geometry_hint.return_value = geo.Rect(x=1,y=2,width=3,height=4)
        widget.geometry = geo.Rect(x=1,y=2,width=3,height=5)
        backend.setup_widget(widget)
        parent._backend_window.subwin.assert_not_called()
        widget._backend_window.mvwin.assert_called_once()
        widget._backend_window.resize.assert_called_once()

    def test_setup_widget_child_existing_noresize(self):
        backend = CursesBackend()
        parent = MagicMock()
        widget = MagicMock()
        widget._backend_window = MagicMock()
        widget.parent = parent
        widget.geometry_hint.return_value = geo.Rect(x=1,y=2,width=3,height=4)
        widget.geometry = geo.Rect(x=1,y=2,width=3,height=4)
        backend.setup_widget(widget)
        parent._backend_window.subwin.assert_not_called()
        widget._backend_window.mvwin.assert_not_called()
        widget._backend_window.resize.assert_not_called()


class MockCursesWindow(object):
    def __init__(self, nlines, ncols, begin_y=0, begin_x=0):
        self.rect = geo.Rect(x=begin_x, y=begin_y, width=ncols, height=nlines)
        self.strings = {}
        self.input_queue = []
        self.refreshed = False
        self.resized = False

    def subwin(self, nlines, ncols, begin_y, begin_x):
        return MockCursesWindow(nlines, ncols, begin_y, begin_x)

    def addstr(self, y, x, text, flags=0):
        self.strings[(x, y)] = (text, flags)

    def border(self, *args):
        pass

    def getbegyx(self):
        return (self.rect.top, self.rect.left)

    def getmaxyx(self):
        return (self.rect.bottom, self.rect.right)

    def getch(self):
        return self.input_queue.pop(0)

    def mvwin(self, new_y, new_x):
        self.rect.top_left = geo.Point(new_x, new_y)

    def resize(self, nlines, ncols):
        self.rect.size = geo.Point(ncols, nlines)
        self.resized = True

    def clear(self):
        self.strings.clear()
        self.refreshed = False

    def refresh(self):
        self.refreshed = True


# TODO make widget tests backend-independent
class TestWidget(test_common.TestCase):
    @patch("curses.newwin", new=lambda *args: MockCursesWindow(*args))
    def windows(self):
        parent = widgets.Widget(None, CursesBackend())
        child = widgets.Widget(parent)
        return parent, child

    @contextmanager
    def no_event(self, widget, type=None):
        old = widget.event

        def noevent(event):
            if type is None or type == event.type:
                raise Exception("Event should not have been called")
            return old(event)

        widget.event = noevent
        try:
            yield
        finally:
            widget.event = old

    @contextmanager
    def must_event(self, widget, type, count=1):
        old = widget.event
        events = []

        def noevent(event):
            if type == event.type:
                events.append(event)
            return old(event)

        widget.event = noevent
        try:
            yield
        finally:
            widget.event = old
            self.assertEqual(len(events), count)
    @patch("curses.newwin", new=lambda *args: MockCursesWindow(*args))
    def test_ctor(self):
        window = MockCursesWindow(640, 480, 1, 2)

        with patch("curses.newwin", return_value=window):
            wid = widgets.Widget(None, CursesBackend())
        self.assertIsNone(wid.parent)
        self.assertEqual(wid.children, [])
        self.assertIsNone(wid.focused_child)
        self.assertTrue(wid.enabled)
        self.assertIs(wid._backend_window, window)

        wid0 = widgets.Widget(None, CursesBackend())
        self.assertIsNone(wid0.parent)
        self.assertEqual(wid0.children, [])
        self.assertIsNone(wid0.focused_child)
        self.assertTrue(wid0.enabled)
        self.assertIsInstance(wid0._backend_window, MockCursesWindow)

        wid1 = widgets.Widget(wid)
        self.assertIs(wid1.parent, wid)
        self.assertIn(wid1, wid.children)
        self.assertEqual(wid1.children, [])
        self.assertIsNone(wid1.focused_child)
        self.assertTrue(wid1.enabled)
        self.assertIsInstance(wid1._backend_window, MockCursesWindow)
        self.assertIsNot(wid1._backend_window, wid._backend_window)
        self.assertEqual(wid1._backend_window.rect, wid._backend_window.rect)

        self.assertRaises(Exception, lambda: widgets.Widget(None))

    def test_has_focus(self):
        parent, child = self.windows()

        self.assertTrue(parent.has_focus())
        parent.enabled = False
        self.assertFalse(parent.has_focus())
        parent.enabled = True

        self.assertFalse(child.has_focus())
        parent.focus(child)
        self.assertTrue(child.has_focus())
        child.enabled = False
        self.assertFalse(child.has_focus())
        child.enabled = True
        parent.focus(None)
        self.assertFalse(child.has_focus())

    def test_focus(self):
        parent, child = self.windows()

        parent.enabled = False
        parent.focus(child)
        self.assertFalse(child.has_focus())
        parent.enabled = True

        child.focus()
        self.assertTrue(child.has_focus())
        parent.focus(None)

        child.focus()
        with self.no_event(child):
            child.focus()
        parent.focus(None)

        sibling = widgets.Widget(parent)
        child.focus()
        self.assertTrue(child.has_focus())
        self.assertFalse(sibling.has_focus())
        with self.must_event(child, widgets.Event.TYPE_FOCUS):
            sibling.focus()
            self.assertTrue(sibling.has_focus())
            self.assertFalse(child.has_focus())
        parent.focus(None)

        parent.focus()

    def test_deactivate(self):
        parent, child = self.windows()
        self.assertTrue(parent.enabled)
        parent.deactivate()
        self.assertFalse(parent.enabled)

    @patch("curses.getmouse", new=lambda: (0, 1, 2, 0, 123))
    def test_listen_events(self):
        parent, child = self.windows()
        parent._backend_window.input_queue = [
            curses.KEY_RESIZE,
            curses.KEY_MOUSE,
            ord(" "),
            curses.KEY_HOME,
            curses.KEY_MOUSE,
        ]

        def event_replace(event):
            parent._last_event = event
        parent.event = event_replace

        parent.backend.listen_events(parent)
        self.assertEqual(parent._last_event.type, widgets.Event.TYPE_RESIZE)
        self.assertEqual(parent._last_event.geometry, parent.geometry)

        parent.backend.listen_events(parent)
        self.assertEqual(parent._last_event.type, widgets.Event.TYPE_MOUSE)
        self.assertEqual(parent._last_event.pos, geo.Point(1, 2))
        self.assertEqual(parent._last_event.buttons, 123)

        parent.backend.listen_events(parent)
        self.assertEqual(parent._last_event.type, widgets.Event.TYPE_TEXT)
        self.assertEqual(parent._last_event.char, " ")

        parent.backend.listen_events(parent)
        self.assertEqual(parent._last_event.type, widgets.Event.TYPE_KEY)
        self.assertEqual(parent._last_event.key, curses.KEY_HOME)

        def getmouse_error():
            raise curses.error()
        with patch("curses.getmouse", new=getmouse_error):
            parent.backend.listen_events(parent)
            self.assertIsNotNone(parent._last_event)
            self.assertEqual(parent._last_event.type, widgets.Event.TYPE_KEY)

    def test_default_event_handlers(self):
        parent, child = self.windows()

        pre = vars(parent)
        parent.text_event(widgets.Event.text_event("x"))
        self.assertEqual(pre, vars(parent))

        pre = vars(parent)
        parent.key_event(widgets.Event.key_event(123))
        self.assertEqual(pre, vars(parent))

        self.assertFalse(parent._backend_window.resized)
        bounds = geo.Rect(x1=1, y1=2, x2=11, y2=22)
        parent.resize_event(widgets.Event.resize_event(bounds))
        self.assertEqual(parent.geometry, bounds)
        self.assertTrue(parent._backend_window.resized)
        parent._backend_window.resized = False
        parent.resize_event(widgets.Event.resize_event(bounds))
        self.assertFalse(parent._backend_window.resized)

        self.assertFalse(child.has_focus())
        child.mouse_event(widgets.Event.mouse_event(geo.Point(), 0))
        self.assertTrue(child.has_focus())

        self.assertFalse(parent._backend_window.refreshed)
        parent.focus_event(widgets.Event.focus_event(True))
        self.assertTrue(parent._backend_window.refreshed)
        parent._backend_window.refreshed = False
        parent.focus_event(widgets.Event.focus_event(False))
        self.assertFalse(parent._backend_window.refreshed)

    def test_event_change_focused_child(self):
        parent, child = self.windows()
        self.assertTrue(parent.event_change_focused_child(child))
        self.assertIs(parent.focused_child, child)

    def test_event_change_focused_child(self):
        parent, child = self.windows()

        self.assertFalse(parent._backend_window.refreshed)
        self.assertFalse(child._backend_window.refreshed)
        parent.refresh()
        self.assertTrue(parent._backend_window.refreshed)
        self.assertTrue(child._backend_window.refreshed)

        parent._backend_window.refreshed = False
        child._backend_window.refreshed = False
        child.enabled = False
        parent.refresh()
        self.assertTrue(parent._backend_window.refreshed)
        self.assertFalse(child._backend_window.refreshed)

    @patch("curses.newwin", new=lambda *args: MockCursesWindow(*args))
    def test_loop(self):
        widget = widgets.Widget(None, CursesBackend())
        widget._renders = 0
        def render(painter):
            widget._renders += 1
            if widget._renders > 4:
                raise Exception("Too many iterations")
        widget.render = render
        widget._backend_window.input_queue = [
            curses.KEY_MOUSE,
            curses.KEY_MOUSE,
            curses.KEY_MOUSE,
            ord("q")
        ]
        widget.text_event = lambda *args: widget.deactivate()
        widget.loop()
        self.assertTrue(widget._backend_window.refreshed)
        self.assertEqual(widget._renders, 4)


class TestWidgetEvent(test_common.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestWidgetEvent, self).__init__(*args, **kwargs)

        with patch("curses.newwin", new=lambda *args: MockCursesWindow(*args)):
            self.parent = widgets.Widget(None, CursesBackend())
            self.child = widgets.Widget(self.parent)
            self.sibling = widgets.Widget(self.parent)

        self.resize_event= CaptureEvent(self.parent, "resize_event")
        self.text_event  = CaptureEvent(self.parent, "text_event")
        self.key_event   = CaptureEvent(self.parent, "key_event")
        self.mouse_event = CaptureEvent(self.parent, "mouse_event")
        self.focus_event = CaptureEvent(self.parent, "focus_event")

        self.child_resize_event= CaptureEvent(self.child, "resize_event")
        self.child_text_event  = CaptureEvent(self.child, "text_event")
        self.child_key_event   = CaptureEvent(self.child, "key_event")
        self.child_mouse_event = CaptureEvent(self.child, "mouse_event")
        self.child_focus_event = CaptureEvent(self.child, "focus_event")

        self.sibling_resize_event= CaptureEvent(self.sibling, "resize_event")
        self.sibling_text_event  = CaptureEvent(self.sibling, "text_event")
        self.sibling_key_event   = CaptureEvent(self.sibling, "key_event")
        self.sibling_mouse_event = CaptureEvent(self.sibling, "mouse_event")
        self.sibling_focus_event = CaptureEvent(self.sibling, "focus_event")

    def setUp(self):
        self.parent.enabled = self.child.enabled = True
        self.parent.focus(self.child)

        self.resize_event.propagate = True
        self.text_event.propagate = True
        self.key_event.propagate = True
        self.mouse_event.propagate = True
        self.focus_event.propagate = True

        self.child_resize_event.propagate = True
        self.child_text_event.propagate = True
        self.child_key_event.propagate = True
        self.child_mouse_event.propagate = True
        self.child_focus_event.propagate = True

        self.sibling_resize_event.propagate = True
        self.sibling_text_event.propagate = True
        self.sibling_key_event.propagate = True
        self.sibling_mouse_event.propagate = True
        self.sibling_focus_event.propagate = True

        self.parent._captured_events = []
        self.child._captured_events = []
        self.sibling._captured_events = []

    def no_focus(self):
        self.parent.focus(None)
        self.child._captured_events = []
        self.sibling._captured_events = []

    def test_not_active(self):
        self.parent.enabled = False
        self.parent.event(widgets.Event.resize_event(geo.Rect()))
        self.parent.event(widgets.Event.text_event("x"))
        self.parent.event(widgets.Event.key_event(123))
        self.parent.event(widgets.Event.mouse_event(geo.Point(), 123))
        self.parent.event(widgets.Event.focus_event(False))
        self.assertEqual(len(self.parent._captured_events), 1)
        self.assertEqual(self.parent._captured_events[0][0], "resize_event")
        self.parent.enabled = True

    def test_unknown_event(self):
        self.parent.event(widgets.Event(-1))
        self.assertEqual(self.parent._captured_events, [])

    def test_focus(self):
        self.parent.event(widgets.Event.focus_event(False))
        self.assertEqual(len(self.parent._captured_events), 1)
        self.assertEqual(len(self.child._captured_events), 0)
        self.assertEqual(self.parent._captured_events[0][0], "focus_event")
        self.assertEqual(self.parent._captured_events[0][1].focus, False)

    def test_key_nofocus(self):
        self.no_focus()
        self.parent.event(widgets.Event.key_event(123))
        self.assertEqual(len(self.parent._captured_events), 1)
        self.assertEqual(len(self.child._captured_events), 0)
        self.assertEqual(self.parent._captured_events[0][0], "key_event")
        self.assertEqual(self.parent._captured_events[0][1].key, 123)

    def test_key_propagate(self):
        self.parent.event(widgets.Event.key_event(123))
        self.assertEqual(len(self.parent._captured_events), 1)
        self.assertEqual(len(self.child._captured_events), 1)
        self.assertEqual(self.parent._captured_events[0][0], "key_event")
        self.assertEqual(self.parent._captured_events[0][1].key, 123)
        self.assertEqual(self.child._captured_events[0][0], "key_event")
        self.assertEqual(self.child._captured_events[0][1].key, 123)

    def test_key_nopropagate(self):
        self.key_event.propagate = False
        self.parent.event(widgets.Event.key_event(123))
        self.assertEqual(len(self.parent._captured_events), 1)
        self.assertEqual(len(self.child._captured_events), 0)
        self.assertEqual(self.parent._captured_events[0][0], "key_event")
        self.assertEqual(self.parent._captured_events[0][1].key, 123)

    def test_text_nofocus(self):
        self.no_focus()
        self.parent.event(widgets.Event.text_event("1"))
        self.assertEqual(len(self.parent._captured_events), 1)
        self.assertEqual(len(self.child._captured_events), 0)
        self.assertEqual(self.parent._captured_events[0][0], "text_event")
        self.assertEqual(self.parent._captured_events[0][1].char, "1")

    def test_text_propagate(self):
        self.parent.event(widgets.Event.text_event("1"))
        self.assertEqual(len(self.parent._captured_events), 1)
        self.assertEqual(len(self.child._captured_events), 1)
        self.assertEqual(self.parent._captured_events[0][0], "text_event")
        self.assertEqual(self.parent._captured_events[0][1].char, "1")
        self.assertEqual(self.child._captured_events[0][0], "text_event")
        self.assertEqual(self.child._captured_events[0][1].char, "1")

    def test_text_nopropagate(self):
        self.text_event.propagate = False
        self.parent.event(widgets.Event.text_event("1"))
        self.assertEqual(len(self.parent._captured_events), 1)
        self.assertEqual(len(self.child._captured_events), 0)
        self.assertEqual(self.parent._captured_events[0][0], "text_event")
        self.assertEqual(self.parent._captured_events[0][1].char, "1")

    def test_mouse_nopropagate(self):
        self.no_focus()
        self.mouse_event.propagate = False
        self.parent.event(widgets.Event.mouse_event(geo.Point(1, 2), 123))

        self.assertEqual(len(self.parent._captured_events), 1)
        self.assertEqual(len(self.child._captured_events), 0)

        self.assertEqual(self.parent._captured_events[0][0], "mouse_event")
        self.assertEqual(self.parent._captured_events[0][1].pos, geo.Point(1, 2))
        self.assertEqual(self.parent._captured_events[0][1].buttons, 123)

        self.assertFalse(self.child.has_focus())

    def test_mouse_nofocus(self):
        self.no_focus()
        self.child_mouse_event.propagate = False
        self.parent.event(widgets.Event.mouse_event(geo.Point(1, 2), 123))
        self.assertEqual(len(self.parent._captured_events), 1)
        self.assertEqual(len(self.child._captured_events), 1)
        self.assertEqual(len(self.sibling._captured_events), 0)

        self.assertEqual(self.child._captured_events[0][0], "mouse_event")
        self.assertEqual(self.child._captured_events[0][1].pos, geo.Point(1, 2))
        self.assertEqual(self.child._captured_events[0][1].buttons, 123)

        self.assertTrue(self.child.has_focus())

    def test_mouse_propagate(self):
        self.parent.event(widgets.Event.mouse_event(geo.Point(1, 2), 123))
        self.assertEqual(len(self.parent._captured_events), 1)
        self.assertEqual(len(self.child._captured_events), 1)
        self.assertEqual(len(self.sibling._captured_events), 1)

        self.assertEqual(self.sibling._captured_events[0][0], "mouse_event")
        self.assertEqual(self.sibling._captured_events[0][1].pos, geo.Point(1, 2))
        self.assertEqual(self.sibling._captured_events[0][1].buttons, 123)

    def test_mouse_skip_inactive(self):
        self.child.enabled = False
        self.parent.event(widgets.Event.mouse_event(geo.Point(1, 2), 123))
        self.assertEqual(len(self.parent._captured_events), 1)
        self.assertEqual(len(self.child._captured_events), 0)
        self.assertEqual(len(self.sibling._captured_events), 1)

    def test_mouse_deny_focus(self):
        self.no_focus()
        self.child_mouse_event.propagate = False
        with patch.object(self.parent, "event_change_focused_child", lambda x: False):
            self.parent.event(widgets.Event.mouse_event(geo.Point(1, 2), 123))
        self.assertFalse(self.child.has_focus())

    def test_resize_nopropagate(self):
        self.resize_event.propagate = False
        rect = geo.Rect(x=0, y=0, width=10, height=10)
        self.parent.event(widgets.Event.resize_event(rect))
        self.assertEqual(len(self.parent._captured_events), 1)
        self.assertEqual(len(self.child._captured_events), 0)
        self.assertEqual(self.parent._captured_events[0][0], "resize_event")
        self.assertEqual(self.parent._captured_events[0][1].geometry, rect)

    def test_resize_propagate(self):
        rect = geo.Rect(x=0, y=0, width=10, height=10)
        self.parent.geometry = rect
        self.parent.event(widgets.Event.resize_event(rect))
        self.assertEqual(len(self.parent._captured_events), 1)
        self.assertEqual(len(self.child._captured_events), 1)
        self.assertEqual(self.child._captured_events[0][0], "resize_event")
        self.assertEqual(self.child._captured_events[0][1].geometry, rect)


class CaptureEvent(object):
    def __init__(self, object, name, propagate=True):
        self.object = object
        self.name = name
        self.propagate = propagate
        self.object._captured_events = []
        setattr(self.object, name, self)

    def __call__(self, event):
        self.object._captured_events.append((self.name, event))
        event.propagate = self.propagate


class NoDrawTestCase(test_common.PatchedTestCase):
    patches = [
        patch("curses.color_pair", return_value=0),
        patch("curses.newwin", new=lambda *args: MockCursesWindow(*args)),
    ]


class TestTabBar(NoDrawTestCase):
    def _setup(self):
        self.widget = widgets.TabBar(
            None,
            CursesBackend(),
            ["foo", "bar", "baz"],
            None,
        )
        self.changed = []
        self.widget.signal_changed = self.changed.append

    def test_key_event(self):
        self.widget._backend_window.input_queue = [
            self.widget.backend.KEY_RIGHT,
            self.widget.backend.KEY_RIGHT,
            self.widget.backend.KEY_RIGHT,
            self.widget.backend.KEY_RIGHT,
            self.widget.backend.KEY_LEFT,
            self.widget.backend.KEY_LEFT,
            self.widget.backend.KEY_HOME,
            self.widget.backend.KEY_END,
            self.widget.backend.KEY_F1,
            ord("q"),
        ]

        with patch.object(self.widget, "text_event", lambda e: self.widget.deactivate()):
            self.widget.loop()
        self.assertEqual(self.changed, ["foo", "bar", "baz", "foo", "baz", "bar", "foo", "baz"])

        self.widget.items = []
        self.widget.current = "foo"
        self.widget.key_event(widgets.Event.key_event(self.widget.backend.KEY_RIGHT))
        self.assertIsNone(self.widget.current)
        self.widget.current = "foo"
        self.widget.key_event(widgets.Event.key_event(self.widget.backend.KEY_HOME))
        self.assertIsNone(self.widget.current)
        self.widget.current = "foo"
        self.widget.key_event(widgets.Event.key_event(self.widget.backend.KEY_END))
        self.assertIsNone(self.widget.current)

    def test_geometry_hint(self):
        rect = geo.Rect(x=4, y=7, width=25, height=84)
        self.assertEqual(
            self.widget.geometry_hint(rect),
            geo.Rect(x=rect.x, y=rect.y, width=rect.width, height=1)
        )

    def test_mouse_event(self):
        event = widgets.Event.mouse_event(geo.Point(5, 0), clicked=[2])
        self.widget.mouse_event(event)
        self.assertTrue(event.propagate)

        event = widgets.Event.mouse_event(geo.Point(5, 0), clicked=[1])
        self.widget.mouse_event(event)
        self.assertFalse(event.propagate)
        self.assertEqual(self.widget.current, "bar")

        event = widgets.Event.mouse_event(geo.Point(99, 0), clicked=[1])
        self.widget.mouse_event(event)
        self.assertFalse(event.propagate)
        self.assertEqual(self.widget.current, "bar")

    def test_no_extra_switch(self):
        self.widget._backend_window.input_queue = [
            curses.KEY_RIGHT,
            curses.KEY_HOME,
            ord("q"),
        ]
        with patch.object(self.widget, "text_event", lambda e: self.widget.deactivate()):
            self.widget.loop()
        self.assertEqual(self.changed, ["foo"])

    @patch("curses.newwin", new=lambda *args: MockCursesWindow(*args))
    def test_default_action(self, *a):
        self.widget = widgets.TabBar(
            None,
            CursesBackend(),
            ["foo", "bar", "baz"],
            None,
        )
        self.widget._backend_window.input_queue = [
            self.widget.backend.KEY_RIGHT,
            self.widget.backend.KEY_RIGHT,
            self.widget.backend.KEY_RIGHT,
            self.widget.backend.KEY_RIGHT,
            self.widget.backend.KEY_LEFT,
            self.widget.backend.KEY_LEFT,
            self.widget.backend.KEY_HOME,
            self.widget.backend.KEY_END,
            ord("q"),
        ]
        self.widget.text_event = lambda *args: self.widget.deactivate()
        self.widget.loop()

    def test_render(self, *a):
        self.widget.current = self.widget.items[0]

        separator_string = (self.widget.separator, curses.A_NORMAL)
        strings = self.widget._backend_window.strings

        self.widget.refresh()
        self.assertEqual(strings[(0, 0)], separator_string)
        self.assertEqual(strings[(1, 0)], ("foo", curses.A_REVERSE))
        self.assertEqual(strings[(4, 0)], separator_string)
        self.assertEqual(strings[(5, 0)], ("bar", curses.A_NORMAL))
        self.assertEqual(strings[(8, 0)], separator_string)
        self.assertEqual(strings[(9, 0)], ("baz", curses.A_NORMAL))
        self.assertEqual(strings[(12, 0)], separator_string)

        with patch.object(self.widget, "has_focus", new=lambda: False):
            self.widget.refresh()
            self.assertEqual(strings[(0, 0)], separator_string)
            self.assertEqual(strings[(1, 0)], ("foo", curses.A_UNDERLINE))
            self.assertEqual(strings[(4, 0)], separator_string)
            self.assertEqual(strings[(5, 0)], ("bar", curses.A_NORMAL))
            self.assertEqual(strings[(8, 0)], separator_string)
            self.assertEqual(strings[(9, 0)], ("baz", curses.A_NORMAL))
            self.assertEqual(strings[(12, 0)], separator_string)


class TestMenu(NoDrawTestCase):
    class Item(widgets.Menu.Item):
        def __init__(self, name, test_obj):
            super(TestMenu.Item, self).__init__(name)
            self.action = lambda: test_obj.activated.append(name)

    def _setup(self):
        self.activated = []
        self.widget = widgets.Menu(
            None,
            CursesBackend(),
            [
                TestMenu.Item("foo", self),
                TestMenu.Item("bar", self),
                TestMenu.Item("baz", self),
            ],
        )

    def test_ctor(self):
        self.assertIs(self.widget.current, self.widget.items[0])

        other = widgets.Menu(None, CursesBackend())
        self.assertIsNone(other.current)

    def test_geometry_hint(self):
        self.assertEqual(
            self.widget.geometry_hint(geo.Rect(x1=1, y1=2, x2=116, y2=92)),
            geo.Rect(x=100, y=2, width=16, height=90),
        )

    def test_key_events(self):
        self.widget._backend_window.input_queue = [
            self.widget.backend.KEY_DOWN,
            self.widget.backend.KEY_ENTER,
            self.widget.backend.KEY_DOWN,
            self.widget.backend.KEY_ENTER,
            ord(" "),
            self.widget.backend.KEY_DOWN,
            self.widget.backend.KEY_ENTER,
            self.widget.backend.KEY_DOWN,
            self.widget.backend.KEY_ENTER,
            self.widget.backend.KEY_UP,
            self.widget.backend.KEY_UP,
            ord(" "),
            self.widget.backend.KEY_HOME,
            ord(" "),
            self.widget.backend.KEY_END,
            ord(" "),
            self.widget.backend.KEY_LEFT,
            ord("f"),
            ord("q"),
        ]

        old_te = self.widget.text_event
        def new_te(ev):
            if ev.char == "q":
                self.widget.deactivate()
            else:
                old_te(ev)

        self.widget.current = None
        with patch.object(self.widget, "text_event", new_te):
            self.widget.loop()

        self.assertEqual(
            self.activated,
            ["foo", "bar", "bar", "baz", "foo", "bar", "foo", "baz"]
        )

    def test_key_events_noitems(self):
        self.widget.items = []

        self.widget.current = 1
        self.widget.key_event(widgets.Event.key_event(curses.KEY_HOME))
        self.assertIsNone(self.widget.current)

        self.widget.current = 1
        self.widget.key_event(widgets.Event.key_event(curses.KEY_END))
        self.assertIsNone(self.widget.current)

        self.widget.current = 1
        self.widget.key_event(widgets.Event.key_event(curses.KEY_UP))
        self.assertIsNone(self.widget.current)

        self.widget.current = 1
        self.widget.key_event(widgets.Event.key_event(curses.KEY_DOWN))
        self.assertIsNone(self.widget.current)

    def test_activate(self):
        self.widget.activate()
        self.assertEqual(self.activated, ["foo"])

        self.activated = []
        self.widget.current = None
        self.widget.activate()
        self.assertEqual(self.activated, [])

    def test_submenu(self):
        sub1 = [
            TestMenu.Item("a1", self),
            TestMenu.Item("a2", self),
            TestMenu.Item("a3", self),
        ]
        sub2 = [
            TestMenu.Item("b1", self),
            TestMenu.Item("b2", self),
            TestMenu.Item("b3", self),
        ]
        sub3 = []

        self.widget.push_submenu(sub1)
        self.assertIn(self.widget.current, sub1)
        self.assertEqual(self.widget.items, sub1)

        self.widget.push_submenu(sub2)
        self.assertIn(self.widget.current, sub2)
        self.assertEqual(self.widget.items, sub2)

        self.widget.push_submenu(sub3)
        self.assertIsNone(self.widget.current)
        self.assertEqual(self.widget.items, [])

        self.widget.pop_submenu()
        self.assertIn(self.widget.current, sub2)
        self.assertEqual(self.widget.items, sub2)

        self.widget.key_event(widgets.Event.key_event(curses.KEY_BACKSPACE))
        self.assertIn(self.widget.current, sub1)
        self.assertEqual(self.widget.items, sub1)

        self.widget.key_event(widgets.Event.key_event(curses.KEY_BACKSPACE))
        self.widget.key_event(widgets.Event.key_event(curses.KEY_BACKSPACE))
        self.assertNotEqual(self.widget.items, sub1)
        self.assertEqual(len(self.widget.items), 3)

        self.assertRaises(OverflowError, self.widget.pop_submenu)

    def test_focus_event(self):
        self.widget.current = None

        self.widget.focus_event(widgets.Event.focus_event(False))
        self.assertIsNone(self.widget.current)

        self.widget.focus_event(widgets.Event.focus_event(True))
        self.assertIn(self.widget.current, self.widget.items)

        self.widget.focus_event(widgets.Event.focus_event(False))
        self.assertIn(self.widget.current, self.widget.items)

        self.widget.current = None
        self.widget.items = []
        self.widget.focus_event(widgets.Event.focus_event(True))
        self.assertIsNone(self.widget.current)

    def test_auto_activate(self):
        self.widget.items[0].auto_activate = True
        self.widget.items[2].auto_activate = True
        self.widget.current = None


        self.widget._backend_window.input_queue = [
            curses.KEY_DOWN,
            curses.KEY_DOWN,
            curses.KEY_DOWN,
            ord("q")
        ]

        self.widget.current = None
        with patch.object(self.widget, "text_event",
                          lambda x: self.widget.deactivate()):
            self.widget.loop()

        self.assertEqual(self.activated, ["foo", "baz"])

    def test__starting_y(self):
        with patch.object(self.widget.__class__, "geometry",
                new=geo.Rect(x=100, y=100, width=10, height=3)):
            self.assertEqual(self.widget._starting_y(), 100)

        with patch.object(self.widget.__class__, "geometry",
                new=geo.Rect(x=100, y=200, width=10, height=5)):
            self.assertEqual(self.widget._starting_y(), 201)

        with patch.object(self.widget.__class__, "geometry",
                new=geo.Rect(x=0, y=0, width=80, height=24)):
            self.assertEqual(self.widget._starting_y(), int((24-3)/2))

    def test_render(self):
        self.widget.current = self.widget.items[0]

        strings = self.widget._backend_window.strings

        self.widget.refresh()
        starting_y = self.widget._starting_y()
        self.assertEqual(strings[(1, starting_y)], ("foo", curses.A_REVERSE))
        self.assertEqual(strings[(1, starting_y+1)], ("bar", curses.A_NORMAL))
        self.assertEqual(strings[(1, starting_y+2)], ("baz", curses.A_NORMAL))

        with patch.object(self.widget, "has_focus", new=lambda: False):
            self.widget.refresh()
            self.assertEqual(strings[(1, starting_y)], ("foo", curses.A_UNDERLINE))
            self.assertEqual(strings[(1, starting_y+1)], ("bar", curses.A_NORMAL))
            self.assertEqual(strings[(1, starting_y+2)], ("baz", curses.A_NORMAL))

    def test_mouse_event(self):
        self.widget.current = None
        ev = widgets.Event.mouse_event(
            geo.Point(1, self.widget._starting_y() + 1),
            clicked=[1]
        )
        self.widget.mouse_event(ev)
        self.assertIs(self.widget.current, self.widget.items[1])
        self.assertFalse(ev.propagate)

        ev = widgets.Event.mouse_event(
            geo.Point(1, 0),
            clicked=[1]
        )
        self.widget.mouse_event(ev)
        self.assertIs(self.widget.current, self.widget.items[1])
        self.assertFalse(ev.propagate)

        self.widget.current = None
        ev = widgets.Event.mouse_event(
            geo.Point(1, self.widget._starting_y() + 1),
            clicked=[2]
        )
        self.widget.mouse_event(ev)
        self.assertIsNone(self.widget.current)
        self.assertTrue(ev.propagate)

    def test_default_action(self):
        item = widgets.Menu.Item("hello")
        self.assertTrue(callable(item.action))
        item.action()

class TestTabWidget(NoDrawTestCase):
    def _setup(self):
        self.widget = widgets.TabWidget(
            None,
            CursesBackend(),
            lambda wid: getattr(wid, "name", "Unnamed Widget")
        )

    def _create_tab(self, name):
        widget = self.widget.create_tab(widgets.Widget)
        widget.name = name
        return widget

    def _create_tabs(self, names=["foo", "bar", "baz"]):
            return [self._create_tab(name) for name in names]


    def test_container_area(self):
        self.assertEqual(
            self.widget.container.geometry,
            geo.Rect(x=0, y=1, width=80, height=23)
        )

    def test_create_tab_noadd(self):
        widget = self.widget.create_tab_noadd()
        self.assertIsInstance(widget, widgets.Widget)
        self.assertNotIn(widget, self.widget.tabs)
        self.assertEqual(len(self.widget.tabs), 0)

    def test_create_tab(self):
        widget = self.widget.create_tab()
        self.assertIsInstance(widget, widgets.Widget)
        self.assertEqual(len(self.widget.tabs), 1)
        self.assertIn(widget, self.widget.tabs)
        self.assertIs(widget, self.widget.current_tab)
        self.assertIs(widget, self.widget.bar.current)

    def test_add_tab(self):
        widget = self.widget.create_tab_noadd()
        self.assertIsInstance(widget, widgets.Widget)
        self.assertNotIn(widget, self.widget.tabs)

        self.widget.add_tab(widget)
        self.assertEqual(len(self.widget.tabs), 1)
        self.assertIn(widget, self.widget.tabs)
        self.assertIs(widget, self.widget.current_tab)
        self.assertIs(widget, self.widget.bar.current)

        self._create_tab("foo")
        self.widget.add_tab(widget)
        self.widget.add_tab(widget)
        self.assertEqual(len(self.widget.tabs), 2)

    def test__switch_current(self):
        tabs = self._create_tabs()
        self.assertEqual(tabs, self.widget.tabs)
        self.widget._switch_current(tabs[1])
        self.assertIs(tabs[1], self.widget.current_tab)
        self.assertIs(tabs[1], self.widget.bar.current)
        self.assertFalse(tabs[0].enabled)
        self.assertTrue(tabs[1].enabled)
        self.assertFalse(tabs[2].enabled)
        self.assertTrue(self.widget._backend_window.refreshed)

        self.widget._backend_window.refreshed = False
        self.widget._switch_current(tabs[1])
        self.assertFalse(self.widget._backend_window.refreshed)

        self.widget._switch_current(None)
        self.assertIsNone(self.widget.current_tab)
        self.assertIsNone(self.widget.bar.current)

        self.widget._switch_current(tabs[1])
        self.assertIs(tabs[1], self.widget.current_tab)
        self.assertIs(tabs[1], self.widget.bar.current)

    def test_close_current(self):
        tabs = self._create_tabs()
        self.widget._switch_current(tabs[1])
        self.widget.close_current()
        self.assertIs(tabs[2], self.widget.current_tab)
        self.assertEqual(len(self.widget.tabs), 2)

        self.widget.current_tab = None
        self.widget.close_current()
        self.assertEqual(len(self.widget.tabs), 2)

        self.widget.current_tab = tabs[2]
        self.widget.close_current()
        self.assertIs(tabs[0], self.widget.current_tab)
        self.assertEqual(len(self.widget.tabs), 1)

        self.widget.close_current()
        self.assertIsNone(self.widget.current_tab)
        self.assertEqual(len(self.widget.tabs), 0)

        self.widget.close_current()

    def test_formatter(self):
        tabs = self._create_tabs()
        self.assertEqual(self.widget.bar.formatter(tabs[0]), "foo")

        self.widget = widgets.TabWidget(None, CursesBackend())
        tabs = self._create_tabs()
        self.assertEqual(self.widget.bar.formatter(tabs[0]), "tab")

    def test_on_activated(self):
        self.widget._activated = None

        def on_activated(tab):
            self.widget._activated = tab

        with patch.object(self.widget, "on_activated", on_activated):
            tabs = self._create_tabs()
            self.widget._activated = None
            self.widget.activate_tab()
            self.assertIs(self.widget._activated, tabs[2])

            self.widget._switch_current(None)
            self.widget.activate_tab()
            self.assertIs(self.widget._activated, tabs[2])

test_common.main()
