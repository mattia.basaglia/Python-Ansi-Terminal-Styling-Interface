#
# Copyright (C) 2016-2017 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from mock import MagicMock

import test_common

from patsi.document import color, palette
from patsi import widgets
from patsi.widgets import geometry as geo
from patsi.widgets import util
from patsi.widgets.frame import Frame
from patsi.widgets.event import MouseEvent
from patsi.widgets.backends.base import Painter, Backend


class TestPoint(test_common.TestCase):
    def assert_point(self, point, x, y):
        self.assertEqual(point.x, x)
        self.assertEqual(point.y, y)

    def test_ctor(self):
        self.assert_point(geo.Point(), 0, 0)
        self.assert_point(geo.Point(1, 2), 1, 2)
        self.assert_point(geo.Point(1, 2, reversed=True), 2, 1)
        self.assert_point(geo.Point((1, 2)), 1, 2)
        self.assert_point(geo.Point(x=1, y=2), 1, 2)

    def test_add(self):
        self.assert_point(geo.Point(1, 2) + geo.Point(30, 40), 31, 42)

        point = geo.Point(1, 2)
        point += geo.Point(30, 40)
        self.assert_point(point, 31, 42)

        self.assert_point(geo.Point(34, 43) - geo.Point(3, 1), 31, 42)

        point = geo.Point(34, 43)
        point -= geo.Point(3, 1)
        self.assert_point(point, 31, 42)

    def test_cmp(self):
        self.assertTrue(geo.Point(1, 2) == geo.Point(1, 2))
        self.assertFalse(geo.Point(1, 2) == geo.Point(1, 3))
        self.assertFalse(geo.Point(3, 2) == geo.Point(1, 2))

        self.assertFalse(geo.Point(1, 2) != geo.Point(1, 2))
        self.assertTrue(geo.Point(1, 2) != geo.Point(1, 3))
        self.assertTrue(geo.Point(3, 2) != geo.Point(1, 2))

    def test_interpolate(self):
        self.assert_point(geo.Point(4, 5).interpolate(geo.Point(2, 3)), 3, 4)
        self.assert_point(geo.Point(4, 5).interpolate(geo.Point(2, 3), 0), 4, 5)
        self.assert_point(geo.Point(4, 5).interpolate(geo.Point(2, 3), 1), 2, 3)

    def test_elements(self):
        point = geo.Point(4, 5)
        self.assertEqual(point[0], point.x)
        self.assertEqual(point["x"], point.x)
        self.assertEqual(point[1], point.y)
        self.assertEqual(point["y"], point.y)
        self.assertRaises(KeyError, lambda: point[2])
        self.assertRaises(KeyError, lambda: point["z"])

        x, y = point
        self.assert_point(point, x, y)
        self.assertEqual(tuple(point), (x, y))

    def test_repr(self):
        point = geo.Point(4, 5)
        self.assertEqual(str(point), str(tuple(point)))


class TestRect(test_common.TestCase):
    def assert_rect(self, rect, x1, y1, x2, y2):
        self.assertEqual(rect.top_left, geo.Point(x1, y1))
        self.assertEqual(rect.bottom_right, geo.Point(x2, y2))

    def test_ctor(self):
        tl = geo.Point(1, 2)
        br = geo.Point(11, 22)
        sz = geo.Point(10, 20)
        self.assert_rect(geo.Rect(pos=tl, size=sz), 1, 2, 11, 22)
        self.assert_rect(geo.Rect(top_left=tl, bottom_right=br), 1, 2, 11, 22)
        self.assert_rect(
            geo.Rect(x=tl.x, y=tl.y, width=sz.x, height=sz.y),
            1, 2, 11, 22
        )
        self.assert_rect(
            geo.Rect(x1=tl.x, y1=tl.y, x2=br.x, y2=br.y),
            1, 2, 11, 22
        )
        self.assert_rect(
            geo.Rect(top=tl.x, left=tl.y, bottom=br.x, right=br.y),
            1, 2, 11, 22
        )
        self.assert_rect(geo.Rect(), 0, 0, 0, 0)

    def test_contains(self):
        rect = geo.Rect(x1=10, y1=10, x2=20, y2=20)
        pin = geo.Point(12, 13)
        pout = geo.Point(14, 2)
        pbound = geo.Point(14, 10)

        self.assertTrue(rect.contains(pin))
        self.assertFalse(rect.contains(pout))
        self.assertTrue(rect.contains(pbound))

        self.assertTrue(rect.contains(pin, False))
        self.assertFalse(rect.contains(pout, False))
        self.assertFalse(rect.contains(pbound, False))

    def test_property_getters(self):
        rect = geo.Rect(x1=1, y1=2, x2=11, y2=32)
        self.assertEqual(rect.top_left, geo.Point(1, 2))
        self.assertEqual(rect.bottom_right, geo.Point(11, 32))
        self.assertEqual(rect.top_right, geo.Point(11, 2))
        self.assertEqual(rect.bottom_left, geo.Point(1, 32))
        self.assertEqual(rect.center, geo.Point(6, 17))

        self.assertEqual(rect.top, 2)
        self.assertEqual(rect.left, 1)
        self.assertEqual(rect.bottom, 32)
        self.assertEqual(rect.right, 11)

        self.assertEqual(rect.x1, 1)
        self.assertEqual(rect.y1, 2)
        self.assertEqual(rect.x2, 11)
        self.assertEqual(rect.y2, 32)

        self.assertEqual(rect.x, 1)
        self.assertEqual(rect.y, 2)
        self.assertEqual(rect.width, 10)
        self.assertEqual(rect.height, 30)

        self.assertEqual(rect.pos, geo.Point(1, 2))
        self.assertEqual(rect.size, geo.Point(10, 30))

    def test_property_setters(self):
        rect = geo.Rect(x1=1, y1=2, x2=11, y2=32)
        rect.top_left = geo.Point(10, 20)
        self.assert_rect(rect, 10, 20, 11, 32)

        rect = geo.Rect(x1=1, y1=2, x2=11, y2=32)
        rect.bottom_right = geo.Point(30, 40)
        self.assert_rect(rect, 1, 2, 30, 40)

        rect = geo.Rect(x1=1, y1=2, x2=11, y2=32)
        rect.top_right = geo.Point(10, 20)
        self.assert_rect(rect, 1, 20, 10, 32)

        rect = geo.Rect(x1=1, y1=2, x2=11, y2=32)
        rect.bottom_left = geo.Point(40, 20)
        self.assert_rect(rect, 40, 2, 11, 20)

        rect = geo.Rect(x1=1, y1=2, x2=11, y2=32)
        rect.left = 4
        rect.top = 5
        rect.right = 6
        rect.bottom = 7
        self.assert_rect(rect, 4, 5, 6, 7)

        rect = geo.Rect(x1=1, y1=2, x2=11, y2=32)
        rect.x = 101
        rect.y = 102
        self.assert_rect(rect, 101, 102, 111, 132)


        rect = geo.Rect(x1=1, y1=2, x2=11, y2=32)
        rect.x1 = 101
        rect.y1 = 102
        self.assert_rect(rect, 101, 102, 11, 32)
        rect.x2 = 201
        rect.y2 = 202
        self.assert_rect(rect, 101, 102, 201, 202)

        rect = geo.Rect(x1=1, y1=2, x2=11, y2=32)
        rect.pos = geo.Point(101, 102)
        self.assert_rect(rect, 101, 102, 111, 132)

        rect = geo.Rect(x1=1, y1=2, x2=11, y2=32)
        rect.width = 100
        rect.height = 200
        self.assert_rect(rect, 1, 2, 101, 202)

        rect = geo.Rect(x1=1, y1=2, x2=11, y2=32)
        rect.size = geo.Point(100, 200)
        self.assert_rect(rect, 1, 2, 101, 202)

        rect = geo.Rect(x1=1, y1=2, x2=11, y2=32)
        rect.center = geo.Point(0, 0)
        self.assert_rect(rect, -5, -15, 5, 15)

    def test_comparison(self):
        self.assertTrue(
            geo.Rect(x1=10, y1=20, x2=30, y2=40) ==
            geo.Rect(x1=10, y1=20, x2=30, y2=40)
        )
        self.assertFalse(
            geo.Rect(x1=10, y1=20, x2=30, y2=40) ==
            geo.Rect(x1=15, y1=20, x2=30, y2=40)
        )
        self.assertFalse(
            geo.Rect(x1=10, y1=20, x2=30, y2=40) ==
            geo.Rect(x1=10, y1=25, x2=30, y2=40)
        )
        self.assertFalse(
            geo.Rect(x1=10, y1=20, x2=30, y2=40) ==
            geo.Rect(x1=10, y1=20, x2=35, y2=40)
        )
        self.assertFalse(
            geo.Rect(x1=10, y1=20, x2=30, y2=40) ==
            geo.Rect(x1=10, y1=20, x2=30, y2=45)
        )

        self.assertFalse(
            geo.Rect(x1=10, y1=20, x2=30, y2=40) !=
            geo.Rect(x1=10, y1=20, x2=30, y2=40)
        )
        self.assertTrue(
            geo.Rect(x1=10, y1=20, x2=30, y2=40) !=
            geo.Rect(x1=15, y1=20, x2=30, y2=40)
        )
        self.assertTrue(
            geo.Rect(x1=10, y1=20, x2=30, y2=40) !=
            geo.Rect(x1=10, y1=25, x2=30, y2=40)
        )
        self.assertTrue(
            geo.Rect(x1=10, y1=20, x2=30, y2=40) !=
            geo.Rect(x1=10, y1=20, x2=35, y2=40)
        )
        self.assertTrue(
            geo.Rect(x1=10, y1=20, x2=30, y2=40) !=
            geo.Rect(x1=10, y1=20, x2=30, y2=45)
        )

    def test_copy(self):
        r1 = geo.Rect(x1=10, y1=20, x2=30, y2=40)
        r2 = r1.copy()
        self.assertIsNot(r1, r2)
        r2.x1 = 100
        r2.y1 = 200
        r2.x2 = 300
        r2.y2 = 400
        self.assert_rect(r1, 10, 20, 30, 40)
        self.assert_rect(r2, 100, 200, 300, 400)

    def test_repr(self):
        rect = geo.Rect(x=10, y=20, width=30, height=40)
        self.assertEqual(str(rect), "Rect(30x40+10+20)")


class TestUtil(test_common.TestCase):
    def test_next_object(self):
        arr = [0, 1, 2, 3, 4]
        curr = 2
        self.assertEqual(4, util.next_object(arr, curr, -3))
        self.assertEqual(0, util.next_object(arr, curr, -2))
        self.assertEqual(1, util.next_object(arr, curr, -1))
        self.assertEqual(2, util.next_object(arr, curr, 0))
        self.assertEqual(3, util.next_object(arr, curr, 1))
        self.assertEqual(4, util.next_object(arr, curr, 2))
        self.assertEqual(0, util.next_object(arr, curr, 3))

        curr = None
        self.assertEqual(2, util.next_object(arr, curr, -3))
        self.assertEqual(3, util.next_object(arr, curr, -2))
        self.assertEqual(4, util.next_object(arr, curr, -1))
        self.assertEqual(0, util.next_object(arr, curr, 0))
        self.assertEqual(0, util.next_object(arr, curr, 1))
        self.assertEqual(1, util.next_object(arr, curr, 2))
        self.assertEqual(2, util.next_object(arr, curr, 3))

        self.assertEqual(curr, util.next_object([], curr, 3))


class MockPainter(Painter):
    def __init__(self, width, height):
        self.styles = []
        self.output = [" "*width] * height
        self.pos = geo.Point()

    def set_style(self, style):
        self.styles.append(style)

    def set_pos(self, pos):
        self.pos = pos

    def draw_styled_text(self, pos, style, text):
        self.set_style(style)
        self.draw_text_at(pos, text)

    def draw_text_at(self, *args):
        if len(args) == 3:
            pos, string = geo.Point(args[0], args[1]), args[2]
        else:
            pos, string = args
        original = self.output[pos.y]
        self.output[pos.y] = original[:pos.x] + string + original[pos.x+len(string):]

    def draw_border(self, frame=Frame()):
        frame.draw(self, geo.Rect(x=0, y=0, width=len(self.output[0]),
                                  height=len(self.output)))

    def draw_text(self, text):
        self.draw_text_at(self.pos, text)


class TestFrame(test_common.TestCase):
    def assert_painted(self, painter, expected):
        self.assertMultiLineEqual(
            "\n".join(painter.output)+"\n",
            expected+"\n"
        )

    def test_default(self):
        painter = MockPainter(5, 3)
        painter.draw_border(Frame())
        self.assert_painted(
            painter,
            "+---+\n|   |\n+---+"
        )

    def test_1char(self):
        painter = MockPainter(5, 3)
        painter.draw_border(Frame(None, *"12345678"))
        self.assert_painted(
            painter,
            "51118\n3   4\n62227"
        )

    def test_wide_top(self):
        painter = MockPainter(6, 3)
        painter.draw_border(Frame(top="[]"))
        self.assert_painted(
            painter,
            "+[][]+\n|    |\n+----+"
        )

    def test_wide_bottom(self):
        painter = MockPainter(6, 3)
        painter.draw_border(Frame(bottom="[]"))
        self.assert_painted(
            painter,
            "+----+\n|    |\n+[][]+"
        )

    def test_wide_left(self):
        painter = MockPainter(6, 3)
        painter.draw_border(Frame(left="[]"))
        self.assert_painted(
            painter,
            "+----+\n[]   |\n+----+"
        )

    def test_wide_right(self):
        painter = MockPainter(6, 3)
        painter.draw_border(Frame(right="[]"))
        self.assert_painted(
            painter,
            "+----+\n|   []\n+----+"
        )

    def test_wide_tl(self):
        painter = MockPainter(6, 3)
        painter.draw_border(Frame(tl="[]"))
        self.assert_painted(
            painter,
            "[]---+\n|    |\n+----+"
        )

    def test_wide_bl(self):
        painter = MockPainter(6, 3)
        painter.draw_border(Frame(bl="[]"))
        self.assert_painted(
            painter,
            "+----+\n|    |\n[]---+"
        )

    def test_wide_br(self):
        painter = MockPainter(6, 3)
        painter.draw_border(Frame(br="[]"))
        self.assert_painted(
            painter,
            "+----+\n|    |\n+---[]"
        )

    def test_wide_tr(self):
        painter = MockPainter(6, 3)
        painter.draw_border(Frame(tr="[]"))
        self.assert_painted(
            painter,
            "+---[]\n|    |\n+----+"
        )

    def test_style_is_set(self):
        painter = MockPainter(5, 3)
        painter = MockPainter(5, 3)
        style = object()
        painter.draw_border(Frame(style))
        self.assertListEqual(painter.styles, [style])
        self.assertIs(painter.styles[0], style)


class TestEvent(test_common.TestCase):
    def test_ctor(self):
        ev = widgets.Event(123, foo="bar")
        self.assertEqual(ev.type, 123)
        self.assertTrue(hasattr(ev, "foo"))
        self.assertEqual(ev.foo, "bar")
        self.assertTrue(ev.propagate)

    def test_accept(self):
        ev = widgets.Event(123)
        ev.accept()
        self.assertFalse(ev.propagate)

    def test_static(self):
        ev = widgets.Event.text_event("x")
        self.assertEqual(ev.type, widgets.Event.TYPE_TEXT)
        self.assertEqual(ev.char, "x")

        ev = widgets.Event.key_event(123)
        self.assertEqual(ev.type, widgets.Event.TYPE_KEY)
        self.assertEqual(ev.key, 123)

        ev = widgets.Event.resize_event(geo.Rect())
        self.assertEqual(ev.type, widgets.Event.TYPE_RESIZE)
        self.assertEqual(ev.geometry, geo.Rect())

        ev = widgets.Event.mouse_event(geo.Point(), 0)
        self.assertEqual(ev.type, widgets.Event.TYPE_MOUSE)
        self.assertEqual(ev.pos, geo.Point())
        self.assertEqual(ev.buttons, 0)

        ev = widgets.Event.focus_event(True)
        self.assertEqual(ev.type, widgets.Event.TYPE_FOCUS)
        self.assertEqual(ev.focus, True)

    def test_close(self):
        close_event = widgets.Event.close_event()
        self.assertEqual(close_event.type, widgets.Event.TYPE_CLOSE)
        self.assertEqual(close_event.reason_code, 0)

        close_event = widgets.Event.close_event(42)
        self.assertEqual(close_event.type, widgets.Event.TYPE_CLOSE)
        self.assertEqual(close_event.reason_code, 42)

    def test_mouse_event(self):
        mouse_event = widgets.Event.mouse_event(
            geo.Point(0, 0),
            MouseEvent.make_flag(1, MouseEvent.MOUSE_RELEASED) |
            MouseEvent.make_flag(1, MouseEvent.MOUSE_PRESSED) |
            MouseEvent.make_flag(2, MouseEvent.MOUSE_CLICKED)
        )

        self.assertEqual(mouse_event.button(1),
                         MouseEvent.MOUSE_RELEASED|MouseEvent.MOUSE_PRESSED)
        self.assertEqual(mouse_event.button(2), MouseEvent.MOUSE_CLICKED)
        self.assertEqual(mouse_event.button(3), 0)


class TestStyle(test_common.TestCase):
    def test_ctor_flags(self):
        style = widgets.Style([
            widgets.Style.STYLE_BLINK,
            widgets.Style.STYLE_BOLD,
            -widgets.Style.STYLE_BLINK,
            widgets.Style.STYLE_BOLD,
        ])
        self.assertListEqual(
           style.styles,
           [widgets.Style.STYLE_BOLD, -widgets.Style.STYLE_BLINK]
        )

    def test_or_flags(self):
        style1 = widgets.Style([widgets.Style.STYLE_BLINK, widgets.Style.STYLE_BOLD])
        style2 = widgets.Style([widgets.Style.STYLE_BLINK, widgets.Style.STYLE_ITALIC])
        self.assertSetEqual(
            set((style1|style2).styles),
            {widgets.Style.STYLE_BLINK, widgets.Style.STYLE_BOLD, widgets.Style.STYLE_ITALIC}
        )

    def test_ior_flags(self):
        style = widgets.Style([widgets.Style.STYLE_BLINK, widgets.Style.STYLE_BOLD])
        style |= widgets.Style([widgets.Style.STYLE_BLINK, widgets.Style.STYLE_ITALIC])
        self.assertSetEqual(
            set(style.styles),
            {widgets.Style.STYLE_BLINK, widgets.Style.STYLE_BOLD, widgets.Style.STYLE_ITALIC}
        )

    def test_or_color(self):
        style1 = widgets.Style([], 1)
        style2 = widgets.Style([], 2)
        self.assertEqual((style1|style2).color, 2)

    def test_ior_color(self):
        style = widgets.Style([], 1)
        style |= widgets.Style([], 2)
        self.assertEqual(style.color, 2)


class TestBackendCommon(test_common.TestCase):
    def assert_not_implemented(self, func, *args, **kwargs):
        self.assertRaises(NotImplementedError, lambda: func(*args, **kwargs))

    def assert_not_implemented_passmocks(self, func, n=0):
        self.assert_not_implemented(func, *[MagicMock() for i in xrange(n)])

    def test_backend_not_implemented(self):
        backend = Backend()
        self.assert_not_implemented_passmocks(backend.setup_widget, 1)
        self.assert_not_implemented_passmocks(backend.get_geometry, 1)
        self.assert_not_implemented_passmocks(backend.set_geometry, 2)
        self.assert_not_implemented_passmocks(backend.listen_events, 1)
        self.assert_not_implemented_passmocks(backend.painter, 1)
        self.assert_not_implemented_passmocks(backend.init, 0)
        self.assert_not_implemented_passmocks(backend.deinit, 0)

    def test_backend_context_manager(self):
        backend = Backend()
        backend.init = MagicMock()
        backend.deinit = MagicMock()
        with backend as ctx:
            self.assertIs(backend, ctx)
            backend.init.assert_called_once()
            backend.deinit.assert_not_called()
        backend.deinit.assert_called_once()

    def test_painter_not_implemented(self):
        painter = Painter()
        self.assert_not_implemented_passmocks(painter.set_style, 1)
        self.assert_not_implemented_passmocks(painter.set_pos, 1)
        self.assert_not_implemented_passmocks(painter.draw_styled_text, 3)
        self.assert_not_implemented_passmocks(painter.draw_border, 1)
        self.assert_not_implemented_passmocks(painter.draw_text, 1)
        self.assert_not_implemented_passmocks(painter.draw_text_at, 3)
        self.assert_not_implemented_passmocks(painter._get_child_painter, 1)

    def test_painter_context_manager(self):
        painter = Painter()
        with painter as ctx:
            self.assertIs(painter, ctx)

    def test_painter_enter(self):
        painter = Painter()
        painter._get_child_painter = MagicMock()
        with painter.enter(MagicMock()) as ctx:
            self.assertIsNot(painter, ctx)
            painter._get_child_painter.assert_called_once()


test_common.main()
