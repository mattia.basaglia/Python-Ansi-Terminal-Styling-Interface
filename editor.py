#!/usr/bin/env python
#
# Copyright (C) 2016-2017 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import os
import sys
import six

from patsi import ansi
from patsi import document
from patsi import widgets
from patsi.widgets.style import Style
from patsi.widgets.frame import Frame


class Editor(widgets.Widget):
    def __init__(self, parent, window):
        super(Editor, self).__init__(parent, window)
        self.document = document.Document()
        self.active_layer = None
        self.file = ""
        self.cursor = widgets.Point()
        self.offset = widgets.Point(1, 1)
        self.message = ""

    def geometry_hint(self, parent_bounds):
        bounds = parent_bounds.copy()
        bounds.y1 = bounds.y1 + 1
        return bounds

    def open(self, file):
        self.document = document.loader.factory.load(file)
        self.file = file
        if self.document.layers:
            self.active_layer = self.document.layers[-1]
        else:
            self.active_layer = None

    def render(self, painter):
        self.render_picture(painter)
        self.render_ui(painter)

    def render_picture(self, painter):
        layer = self.document.flattened()
        win_width, win_height = self.geometry.size

        for pos, item in six.iteritems(layer.matrix):
            pos = widgets.Point(pos) + self.offset
            if pos.x < 1 or pos.y < 1 or pos.x >= win_width - 1 or pos.y >= win_height - 1:
                continue
            char, col = item
            painter.draw_text(pos.y, pos.x, Style(color=col), char)

    def render_ui(self, painter):
        win_width, win_height = self.geometry.size

        painter.draw_border(Frame(
            left = "|" if self.offset.x >= 1 else ":",
            right = "|" if self.offset.x + self.document.width < win_width - 1 else ":",
            top = "_" if self.offset.y >= 1 else ".",
            bottom = "_" if self.offset.y + self.document.height < win_height - 1 else ".",
            tl=" ", tr=" ", bl="|", br="|"
        ))

        def footer(str, *args, **kwargs):
            painter.draw_text(footer, str, *args, **kwargs)
            footer.x += len(str) + 1
        footer.x = 1
        footer.y = win_height - 1

        footer("%2s, %2s" % (self.cursor.x, self.cursor.y))
        footer(self._active_color_name(), self._active_color())
        if self.message:
            footer(" -- %s" % self.message)

    def move_cursor(self, window):
        if self.has_focus():
            point = self.cursor + self.offset + self.geometry.pos
            window.move(point.y, point.x)
            window.chgat(1, self._active_color())

    def _active_color(self):
        if self.active_layer:
            return self.active_layer.color
        return None

    def _active_color_name(self):
        if self.active_layer:
            return self.active_layer.color.name
        return ""

    def set_char(self, char):
        if self.active_layer:
            self.active_layer.set_char(self.cursor.x, self.cursor.y, char)

    def text_event(self, event):
        self.set_char(event.char)
        self.cursor.x += 1
        self._adjust_cursor()

    def key_event(self, event):
        if event.key == self.backend.KEY_LEFT:
            self.cursor.x -= 1
        elif event.key == self.backend.KEY_RIGHT:
            self.cursor.x += 1
        elif event.key == self.backend.KEY_UP:
            self.cursor.y -= 1
        elif event.key == self.backend.KEY_DOWN or event.key == self.backend.KEY_ENTER:
            self.cursor.y += 1
        elif event.key == self.backend.KEY_HOME:
            self.cursor.x = 0
        elif event.key == self.backend.KEY_END:
            self.cursor.x = self.document.width
        elif event.key == self.backend.KEY_SHOME:
            self.cursor.y = 0
        elif event.key == self.backend.KEY_SEND:
            self.cursor.y = self.document.height
        elif event.key == self.backend.KEY_BACKSPACE:
            self.cursor.x -= 1
            self.set_char(" ") # TODO Remove the char
        elif event.key == self.backend.KEY_DC:
            self.set_char(" ") # TODO Remove the char
            self.cursor.x += 1
        self._adjust_cursor()
        event.accept()

    def resize_event(self, event):
        super(Editor, self).resize_event(event)
        self._adjust_cursor()

    def _adjust_cursor(self):
        if self.cursor.x < 0:
            self.cursor.x = 0

        if self.cursor.y < 0:
            self.cursor.y = 0

        win_width, win_height = self.geometry.size

        if self.cursor.y + self.offset.y >= win_height - 1:
            self.offset.y = win_height - 2 - self.cursor.y
        elif self.cursor.y + self.offset.y < 1:
            self.offset.y = 1 - self.cursor.y

        if self.cursor.x + self.offset.x >= win_width - 1:
            self.offset.x = win_width - 2 - self.cursor.x
        elif self.cursor.x + self.offset.x < 1:
            self.offset.x = 1 - self.cursor.x

        self.refresh()

    @property
    def name(self):
        return self.document.name or os.path.basename(self.file)

    def mouse_event(self, event):
        super(Editor, self).mouse_event(event)
        if event.clicked(1):
            self.cursor = event.pos - self.offset
            self._adjust_cursor()
            event.accept()


class Manager(widgets.TabWidget):
    def __init__(self, backend):
        super(Manager, self).__init__(None, backend, lambda e: e.name)
        self.backend.show_cursor(False)

        self.menu = widgets.Menu(self, None, [
            widgets.Menu.Item("Select Layer", self._menu_select_layer),
            widgets.Menu.Item("Layer Actions"),
            widgets.Menu.Item("Save"),
            widgets.Menu.Item("Open"),
            widgets.Menu.Item("Close", self.close_current),
            widgets.Menu.Item("Exit", self.deactivate),
        ])
        self.focus(self.menu)

    def _switch_layer(self, layer):
        if not self.current_tab or not self.current_tab.document.layers:
            return
        self.current_tab.active_layer = layer
        self.refresh()

    def _menu_layer_item(self, layer):
        return widgets.Menu.Item(
            layer.color.name,
            lambda: self._switch_layer(layer),
            widgets.Style([], layer.color)
        )

    def _menu_select_layer(self):
        if self.current_tab:
            self.menu.push_submenu([
                self._menu_layer_item(layer)
                for layer in self.current_tab.document.layers
            ])

    def open_tab(self, file=None):
        editor = self.create_tab_noadd(Editor)
        if file is not None:
            try:
                editor.open(file)
            except Exception:
                return
        self.add_tab(editor)

    def on_activated(self, tab):
        self.backend.show_cursor(True)
        self.menu.enabled = False

    def key_event(self, event):
        if event.key == 0x1b: # Escape
            if self.current_tab and self.container.has_focus():
                self.menu.enabled = True
                self.focus(self.menu)
            else:
                self.activate_tab()
            self.refresh()
            event.accept()

    def refresh(self):
        super(Manager, self).refresh()
        if self.current_tab:
            self.current_tab.move_cursor(self._backend_window)


if __name__ == "__main__":
    with widgets.backends.curses.CursesBackend() as backend:
        manager = Manager(backend)
        for file in sys.argv[1:]:
            manager.open_tab(file)
        with ansi.keyboard_interrupt():
            manager.loop()
